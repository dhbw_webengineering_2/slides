FROM node:16-alpine 

WORKDIR /app
COPY . .

RUN npm ci 
RUN npm run build

EXPOSE 8080

# gulp-webserver needs to be started under 0.0.0.0, that it can be exposed by docker
CMD ["npm", "run", "start", "--", "--host=0.0.0.0"]